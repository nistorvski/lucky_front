import React, { useState, Fragment } from "react";
import { registerShelter } from "../../api/authShelter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImages } from "@fortawesome/free-solid-svg-icons";

import Buttons from "../../components/Buttons/Buttons";
import "./Register.scss";

const ShelterRegister = (props) => {
  const shelterData = {
    name: "",
    email: "",
    password: "",
    avatar: null,
  };

  const [shelter, setShelter] = useState(shelterData);

  const [error, setError] = useState("");

  const handleChangeInput = (ev) => {
    const { name, value } = ev.target;

    if (name === "avatar") {
      let reader = new FileReader();
      let file = ev.target.files[0];

      reader.onloadend = () => {
        setShelter({ ...shelter, avatar: file });
      };
      reader.readAsDataURL(file);

      console.log(file);
    } else {
      setShelter({ ...shelter, [name]: value });
    }
  };

  const handleFormSubmit = async (ev) => {
    ev.preventDefault();
    try {
      const form = new FormData();

      form.append("name", shelter.name);
      form.append("email", shelter.email);
      form.append("password", shelter.password);
      form.append("avatar", shelter.avatar);

      const data = await registerShelter(form);

      console.log("Registro completadoo", data);
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <Fragment>
      <div className="register">
        <div className="register__logo">
          <img src="/images/logo_img/logo.png" alt="" />
        </div>

        <div className="register__title">
          <p>¡Hola! para continuar, sigue los pasos para crear una cuenta</p>
        </div>

        <form
          encType="multipart/form-data"
          onSubmit={handleFormSubmit}
          className="register__form"
        >
          <label htmlFor="name" className="register__form__label">
            <input
              className="register__form__input"
              type="text"
              name="name"
              value={shelter.name}
              placeholder="Username"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="email" className="register__form__label">
            <input
              className="register__form__input"
              type="email"
              name="email"
              value={shelter.email}
              placeholder="Email"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="password" className="register__form__label">
            <input
              className="register__form__input"
              type="password"
              name="password"
              value={shelter.password}
              placeholder="Password"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="avatar" className="register__form__label">
            <FontAwesomeIcon className="register__form__file" icon={faImages} />
            <p className="register__form__paragraf">Añade avatar</p>
            <input
              style={{ display: "none" }}
              className="register__form__input"
              type="file"
              id="avatar"
              name="avatar"
              placeholder="Avatar"
              onChange={handleChangeInput}
              style={{ display: "none" }}
            />
          </label>

          <Buttons type="submit" style={"blue"} children={"Registrar"} />
            
        </form>
      </div>
    </Fragment>
  );
};

export default ShelterRegister;
