import React from'react';
import { Link } from 'react-router-dom';
import Buttons from '../../components/Buttons/Buttons';
import './Register.scss'

const RegisterChoose = () => {


        return(
            <div className="registerChoose">
                
                <div className="registerChoose__title">
                    <h3>¿Cómo quieres entrar?</h3>
                </div>

              <Link to="/user-register"> <Buttons style={"blue"} children={"Usuario"} /></Link> 
                <Buttons style={"blue"} children={"Asociación protectora"} />

                <div className="registerChoose__link">
                    <a href="#">Registrarse en otro momento</a>
                </div>


            </div>
        )  
    }


export default RegisterChoose;