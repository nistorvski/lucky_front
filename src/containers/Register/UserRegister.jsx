import React, { useState, Fragment } from "react";
import { register } from "../../api/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImages } from "@fortawesome/free-solid-svg-icons";

import Buttons from "../../components/Buttons/Buttons";
import "./Register.scss";

const UserRegister = (props) => {
  const userData = {
    name: "",
    email: "",
    password: "",
    avatar: null,
  };

  const [users, setUsers] = useState(userData);

  const [error, setError] = useState("");

  const handleChangeInput = (ev) => {
    const { name, value } = ev.target;

    if (name === "avatar") {
      let reader = new FileReader();
      let file = ev.target.files[0];

      reader.onloadend = () => {
        setUsers({ ...users, avatar: file });
      };
      reader.readAsDataURL(file);

      console.log(file);
    } else {
      setUsers({ ...users, [name]: value });
    }
  };

  const handleFormSubmit = async (ev) => {
    ev.preventDefault();
    try {
      const form = new FormData();

      form.append("name", users.name);
      form.append("email", users.email);
      form.append("password", users.password);
      form.append("avatar", users.avatar);

      const data = await register(form);

      console.log("REgistro completadoo", data);
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <Fragment>
      <div className="register">
        <div className="register__logo">
          <img src="/images/logo_img/logo.png" alt="" />
        </div>

        <div className="register__title">
          <p>¡Hola! para continuar, sigue los pasos para crear una cuenta</p>
        </div>

        <form
          encType="multipart/form-data"
          onSubmit={handleFormSubmit}
          className="register__form"
        >
          <label htmlFor="name" className="register__form__label">
            <input
              className="register__form__input"
              type="text"
              name="name"
              value={users.name}
              placeholder="Username"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="email" className="register__form__label">
            <input
              className="register__form__input"
              type="email"
              name="email"
              value={users.email}
              placeholder="Email"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="password" className="register__form__label">
            <input
              className="register__form__input"
              type="password"
              name="password"
              value={users.password}
              placeholder="Password"
              onChange={handleChangeInput}
            />
          </label>

          <label htmlFor="avatar" className="register__form__label">
            <FontAwesomeIcon className="register__form__file" icon={faImages} />
            <p className="register__form__paragraf">Añade avatar</p>
            <input
              style={{ display: "none" }}
              className="register__form__input"
              type="file"
              id="avatar"
              name="avatar"
              placeholder="Avatar"
              onChange={handleChangeInput}
              style={{ display: "none" }}
            />
          </label>

          <Buttons type="submit" style={"blue"} children={"Registrar"} />
        </form>
      </div>
    </Fragment>
  );
};

export default UserRegister;
