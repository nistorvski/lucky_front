import React, { useState } from "react";
import { loginUser } from "../../api/auth";
import { Link, Redirect } from "react-router-dom";
import Buttons from "../../components/Buttons/Buttons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import "./Login.scss";

const INITIAL_STATE = {
  email: "",
  password: "",
  error: "",
};

const UserLogin = () => {
  const [login, setLogin] = useState(INITIAL_STATE);

  const [shown, setShown] = useState(false);

  const switchShown = () => {
    if (!shown) {
      setShown({ shown: true });
    } else {
      setShown({ shown: false });
    }
  };

  const handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    setLogin({
      ...login,
      [name]: value,
    });
  };

  const handleFormSubmit = async (ev) => {
    ev.preventDefault();
    try {
      const data = await loginUser(login);

      setLogin(INITIAL_STATE);

      console.log("Login completado", data);
    } catch (error) {
      setLogin({
        error: error.message,
      });
    }
  };

  return (
    <div className="loginUser">
      <div className="loginUser__logo">
        <img src="/images/logo_img/logo.png" alt="" />
      </div>

      <div className="loginUser__title">
        <p>¡Hola! para continuar, sigue los pasos para crear una cuenta</p>
      </div>

      <form onSubmit={handleFormSubmit} className="loginUser__form">
        <label htmlFor="email" className="loginUser__form__label">
          <input
            className="loginUser__form__input"
            type="email"
            name="email"
            placeholder="Email"
            value={login.email}
            onChange={handleChangeInput}
          />
        </label>

        <label htmlFor="password" className="loginUser__form__label">
          <input
            className="loginUser__form__input"
            type={shown ? "text" : "password"}
            name="password"
            placeholder="Password"
            value={login.password}
            onChange={handleChangeInput}
          />

          <span onClick={switchShown} className="loginUser__form__see-pass">
            {" "}
            <FontAwesomeIcon
              className="loginUser__form__eye"
              icon={faEye}
            />{" "}
          </span>

          <Link to="/#" className="loginUser__form__change-pass">
            {" "}
            ¿Has olvidado la contraseña?
          </Link>
        </label>

        <Buttons type="submit" style={"blue"} children={"Login"} />
      </form>
    </div>
  );
};

export default UserLogin;
