import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { checkSession } from "./api/auth";
import RegisterChoose from "./containers/Register/RegisterChoose";
import UserRegister from "./containers/Register/UserRegister";
import UserLogin from "./containers/Login/UserLogin";
import ShelterRegister from "./containers/Register/ShelterRegister";
import Navbar from "./components/Navbar/Navbar";
import Profile from "./pages/Profile/Profile";
import Ms from "./pages/Ms/Ms";
import SecureRoute from "./components/SecureRoute/SecureRoute";
import Home from "./pages/Home";
import Gallery from "./components/Gallery/Gallery";
import FormAdoption from "./components/FormAdoption/FormAdoption";
import FormPets from "./components/FormPets/FormPets";
import FormFamily from "./components/FormFamily/FormFamily";
import Splash from "./components/Splash/Splash";

import PetsList from "./components/PetsList";
import PetCard from "./components/PetCard";
import Search from "./components/Search";

import "./App.scss";

const App = (props) => {
  const [error, setError] = useState("");
  const [user, setUser] = useState({});
  const [hasUser, setHasUser] = useState(null);

  useEffect(() => {
    checkUserSession();
  }, []);

  const checkUserSession = async () => {
    try {
      const data = await checkSession();
      delete data.password;

      setUser(data);
      setHasUser(true);

      console.log("CheckSessiooooooooon", data);
    } catch (error) {
      setError({
        error: error.message,
      });
      setHasUser(false);
    }
  };

  const saveUser = (user) => {
    delete user.password;

    setUser(user);
    setHasUser(true);
    setError("");
  };

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/register-choose" component={RegisterChoose} />
          <Route
            exact
            path="/user-register"
            component={(props) => (
              <UserRegister {...props} saveUser={saveUser} />
            )}
          />
          <Route
            exact
            path="/login-register"
            component={(props) => <UserLogin {...props} saveUser={saveUser} />}
          />

          <Route
            exact
            path="/shelter-register"
            component={(props) => (
              <ShelterRegister {...props} saveUser={saveUser} />
            )}
          />

          <SecureRoute
            exact
            path="/profile"
            hasUser={hasUser}
            component={(props) => <Profile {...props} user={user} />}
          />

          <SecureRoute
            exact
            path="/ms"
            hasUser={hasUser}
            component={(props) => (
              <Ms
                {...props}
                user={user}
                setuser={setUser}
                setHasUser={setHasUser}
              />
            )}
          />
          
          {/* <SecureRoute
            exact
            path="/"
            hasUser={hasUser}
            component={(props) => <Navbar {...props} user={user} />}
          /> */}
          {hasUser && <Navbar user={user}/>}
          <Route path="/pets" exact component={PetsList} />
          <Route path="/pets/:petId" exact component={PetCard} />
          <Route path="/search" exact component={Search} />
          <Route path="/gallery" exact component={Gallery} />
          <Route path="/" exact component={Home} />
          <Route path="/formAdoption" exact component={FormAdoption} />
          <Route path="/formPets" exact component={FormPets} />
          <Route path="/formFamily" exact component={FormFamily} />
          <Route path="/petslist" exact component={PetsList} />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
