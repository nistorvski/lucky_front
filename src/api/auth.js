const registerUrl = "https://upgrade-lucky-app.herokuapp.com/user/register";
const loginUrl = "https://upgrade-lucky-app.herokuapp.com/user/login";
const checkSessionUrl = "https://upgrade-lucky-app.herokuapp.com/user/check-session";
const logoutUrl = "https://upgrade-lucky-app.herokuapp.com/user/logout";
const getUserUrl = "https://upgrade-lucky-app.herokuapp.com/user";


export const register = async (userData) => {
  const request = await fetch(registerUrl, {
        method:"POST",
        headers: {
            "Accept":"application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
            
        },
        credentials:"include",
        body: userData,
    })

    //console.log("request", request);

    const response = await request.json();


    if(!request.ok) {
        throw new Error(response.message);
    }

    //console.log("response", response);
    return response; 

}



export const loginUser = async (userData) => {
    const request = await fetch(loginUrl, {
        method:"POST",
        headers: {
            "Accept":"application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials:"include",
        body: JSON.stringify(userData),
    })

    //console.log("request", request);

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
    }

    //console.log("response", response);
    return response.data; 
}

export const checkSession = async () => {
    const request = await fetch(checkSessionUrl, {
      method: 'GET',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      credentials: 'include',
    });
  
    const response = await request.json();
    if(!request.ok) {
      throw new Error(response.message);
    }
  
    return response;
  }

  export const logout = async () => {
    const request = await fetch(logoutUrl, {
      method: 'POST',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      credentials: 'include',
    });
  
    const response = await request.json();
  
    if(!request.ok) {
      throw new Error(response.message);
    }
  
    return response;
  }
  

  export const getUser = async () => {

  }
  