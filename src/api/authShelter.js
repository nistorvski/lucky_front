const registerUrl = "http://localhost:4009/shelter/register";
const loginUrl = "http://localhost:4009/shelter/login";
const checkSessionUrl = "http://localhost:4009/shelter/check-session";
const logoutUrl = "http://localhost:4009/shelter/logout";


export const registerShelter = async (userData) => {
    const request = await fetch(registerUrl, {
        method:"POST",
        headers: {
            "Accept":"application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials:"include",
        body: JSON.stringify(userData),
    })

    //console.log("request", request);

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
    }

    //console.log("response", response);
    return response.data; 

}



export const loginShelter = async (userData) => {
    const request = await fetch(loginUrl, {
        method:"POST",
        headers: {
            "Accept":"application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials:"include",
        body: JSON.stringify(userData),
    })

    //console.log("request", request);

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
    }

    //console.log("response", response);
    return response.data; 
}

export const checkSessionShelter = async () => {
    const request = await fetch(checkSessionUrl, {
      method: 'GET',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      credentials: 'include',
    });
  
    const response = await request.json();
    if(!request.ok) {
      throw new Error(response.message);
    }
  
    return response;
  }

  export const logoutShelter = async () => {
    const request = await fetch(logoutUrl, {
      method: 'POST',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      credentials: 'include',
    });
  
    const response = await request.json();
  
    if(!request.ok) {
      throw new Error(response.message);
    }
  
    return response;
  }
  
  