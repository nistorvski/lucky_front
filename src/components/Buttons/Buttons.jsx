import React from 'react';
import './Buttons.scss';


const getClasses = style => {
    if(style === 'secondary') return 'button--icon';
    if(style === 'pink') return 'button--pink';
    if(style === 'disabled') return 'button--disabled';
    return 'button--blue';

    
}


const Buttons = props => {
 
        return(
            <button 
            type={props.type}
            onClick={props.onClick}
            disabled={props.disabled} 
            className={`button ${getClasses(props.style)}`}>
               {props.icon ? <img src={props.icon} alt="icon" /> : null}
               {props.children}
            </button>
        )
    
}

export default Buttons; 