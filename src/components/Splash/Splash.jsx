import React from "react";
import "./Splash.scss";

const Splash = () => {
  return (
    <div className="logo-face">
      <img
        src="/img/logo.png"
        width="100px"
        height="100px"
        alt="logo de perro"
      />
      <img src="/img/lucky.png" width="100px" height="100px" alt="" />
    </div>
  );
};

export default Splash;
