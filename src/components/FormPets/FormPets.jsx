import React from "react";
import "./FormPets.scss";
import { Link } from "react-router-dom";
import FormAdoption from "../FormAdoption/FormAdoption";

function FormPets() {
  // class FormPets extends Component {
  //   handleFormSubmit = (ev) => {
  //     ev.preventDefault();
  //   };

  //   handleChangeInput = (e) => {
  //     this.setState({
  //       [e.target.name]: e.target.value,
  //     });
  //   };
  // render() {
  return (
    <div>
      <div className="form">
        <h3>Formulario de Adopcion</h3>
      </div>
      <div className="barra">
        <div className="barra_carga_pets"></div>

        <div className="barra_carga_pets2"></div>
      </div>
      <div className="pets">
        <h2>Sobre las Mascotas</h2>
      </div>
      <form>
        <div className="tienes_animales">
          <label>
            <p>
              ¿tienes otros animales?
              <div className="mas__animales">
                <div className="mas__no">
                  no
                  <input type="radio" name="y_n" value="no" placeholder="no" />
                </div>
                <div className="mas__si">
                  si
                  <input type="radio" name="y_n" value="si" placeholder="si" />
                </div>
              </div>
            </p>
          </label>
          <p>
            <label>
              <input type="text" placeholder=" ¿cuales?" />
            </label>
            <label>
              <input
                type="text"
                placeholder="¿se lleva bien con otros animales?"
              />
            </label>
          </p>
          <label>
            <p>¿porque has elegido adoptar?</p>
            <input type="text" />
          </label>
          <label>
            <p> ¿conoces las necesidades del animal?</p>
            <input type="text" />
          </label>
          <label>
            <p> ¿conoces sus gastos?</p>
            <input type="text" />
          </label>
          <label>
            <p> ¿conoces su alimentacion?</p>
            <input type="text" />
          </label>
        </div>
        <Link to="formAdoption">
          <button onKeyPress={() => this.props.navigation.goBack(FormAdoption)}>
            Previous
          </button>
        </Link>

        <br />
        <div className="onSubmit">
          <Link to="formFamily">
            <button className="cont" type="continuar">
              continuar
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
}

export default FormPets;
