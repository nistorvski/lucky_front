import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
//import Search from "../Search";

import "./PetsList.scss";

const PetsList = () => {
  let [petList, setPetList] = useState([]);
  const [input, setInput] = useState("");

  const API_URL = "http://localhost:4003/pets";

  useEffect(() => {
    fetch(API_URL)
      .then((response) => response.json())
      .then((data) => {
        setPetList(data);
      });
  }, []);

  const handleChange = (e) => {
    e.preventDefault();
    setInput(e.target.value);
  };

  if(input.length > 0) {
    petList = petList.filter((pet) => {
      return pet.name.toLowerCase().match(input);
    })
  }


  return (
    <div className="petlist">
      <input
          className="players__search"
          type="text"
          placeholder="Search"
          onChange={handleChange}
          value={input}
          />
      {petList.map((pet) => (
        <div key={JSON.stringify(pet)}>
          <Link to={{
            pathname: `/pets/${pet._id}`,
            state: pet
          }}>
            <div className="petlist__card">
              <img
                src={process.env.PUBLIC_URL + "/favoritos.png"}
                alt=""
                className="petlist__fav"
              />
              {/* FALTA ROUTE DE FAV */}
              <img src={pet.photo} alt={pet.name} className="petlist__img" />

              <div className="petlist__data">
                <p className="petlist__name">{pet.name}</p>
                <div className="petlist__detail">
                  <p className="petlist__text">{pet.specie}</p>
                  <p className="petlist__text">{pet.zone}</p>
                </div>
              </div>
            </div>
          </Link>
        </div>
      ))}
    </div>
  );
}

export default PetsList;
