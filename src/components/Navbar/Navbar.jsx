import React, { useState } from "react";
import { Link } from "react-router-dom";

import "./Navbar.scss";

import iconHome from "../../images/icons/homeicon/blueHome/home@3x.png";
import pinkIconHome from "../../images/icons/homeicon/home@3x.png";

import iconMap from "../../images/icons/mapicon/mapa@3x.png";
import pinkIconMap from "../../images/icons/mapicon/pinkMap/mapa@3x.png";

import petIcon from "../../images/icons/mascotaicon/mascota@3x.png";
import pinkIconPet from "../../images/icons/mascotaicon/pinkPet/mascota@3x.png";

import msIcon from "../../images/icons/mSicon/mS@3x.png";
import pinkMsIcon from "../../images/icons/mSicon/pinkMs/mS@3x.png";

const Navbar = (props) => {
  const [navBtn, setNavBtn] = useState("");

  return (
    <nav className="navbar">
      <div className="navbar__box">
        <Link className="navbar__box__link" to="#">
          <img
            className="navbar__box__image"
            src={navBtn === "Home" ? pinkIconHome : iconHome}
            alt="Home"
            onClick={() => setNavBtn("Home")}
          />
        </Link>
        <Link className="navbar__box__link" to="#">
          <img
            className="navbar__box__image"
            src={navBtn === "Map" ? pinkIconMap : iconMap}
            alt="Map"
            onClick={() => setNavBtn("Map")}
          />
        </Link>
        <Link className="navbar__box__link" to="#">
          <img
            className="navbar__box__image"
            src={navBtn === "Pet" ? pinkIconPet : petIcon}
            alt="Pet"
            onClick={() => setNavBtn("Pet")}
          />
        </Link>
        <Link className="navbar__box__link" to="/profile">
          <img
            className={navBtn === "avatar-pink" ? "avatar-pink" : "avatar"}
            src={props.user.avatar}
            alt="avatar"
            onClick={() => setNavBtn("avatar-pink")}
          />
        </Link>
        <Link className="navbar__box__link" to="/ms">
          <img
            className="navbar__box__image"
            src={navBtn === "Ms" ? pinkMsIcon : msIcon}
            alt="Pet"
            onClick={() => setNavBtn("Ms")}
          />
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
