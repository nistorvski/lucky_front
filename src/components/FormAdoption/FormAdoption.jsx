import React from "react";
import "./FormAdoption.scss";
import { Link } from "react-router-dom";
import { useState } from "react";
import { setState } from "react";

function FormAdoption() {
  // state = {
  //   name: "",
  //   email: "",
  //   telephone: "",
  //   dni: "",
  //   direcction: "",
  //   pd: "",
  //   city: "",
  // };row;

  const handleSubmitForm = (ev) => {
    ev.preventDefault();
  };

  const handleChangeInput = (ev) => setState(ev.target.value);

  function Form({ handleCreateMessage }) {
    const [value, setValue] = useState("");
  }

  function onChangeInput(ev) {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  }

  return (
    <form onSubmit={handleSubmitForm}>
      <div>
        <div className="form">
          <h3>Formulario de adopcion</h3>
        </div>
        <div className="barra">
          <div className="barra_carga_adopcion1"></div>

          <div className="barra_carga_adopcion2"></div>
        </div>
        <div className="your_date">
          <h2>Tus datos</h2>
        </div>
        <div className="form_yuor_date">
          <label className="name">
            <input
              type="text"
              name="name"
              onChange={handleChangeInput}
              placeholder="nombrey apellidos"
            />
          </label>
          <label className="email">
            <input
              type="text"
              name="email"
              onChange={handleChangeInput}
              placeholder="email"
            />
          </label>
          <label className="telefono">
            <input
              type="number"
              name="telephone"
              onChange={handleChangeInput}
              placeholder="telefono"
            />
          </label>
          <label className="dni">
            <input
              type="text"
              name="dni"
              onChange={handleChangeInput}
              placeholder="DNI"
            />
          </label>
          <div className="direction">
            <h2>Direccion</h2>
          </div>
          <label htmlFor="calle">
            <input
              type="text"
              name="direccion"
              onChange={handleChangeInput}
              placeholder="calle, numero, piso"
            />
          </label>
          <label htmlFor="postal code">
            <input
              type="text"
              name="cp"
              onChange={handleChangeInput}
              placeholder="C.P."
            />
          </label>
          <label htmlFor="city">
            <input
              type="text"
              name="city"
              onChange={handleChangeInput}
              placeholder="City"
            />

            <label>
              <div className="validation">
                <h6>Acepto los terminos y condiciones de la adopcion</h6>

                <input type="checkbox" validation="true" require />
              </div>
            </label>
            <br />
            <div className="onSubmit">
              <Link to="formPets">
                <button className="continue" type="continuar">
                  continuar
                </button>
              </Link>
            </div>
          </label>
        </div>
      </div>
    </form>
  );
}

export default FormAdoption;

//

//   return <form onSubmit={this.handleFormSubmit}></form>;
// };
