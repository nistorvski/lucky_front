import React from'react';
import { Route, Redirect } from 'react-router-dom';

const SecureRoute = (props) =>  {

   
        const { hasUser } = props;
        
        if(hasUser === null) {
           return <div>
                CARGANDOO...
            </div>
        }else{
            return hasUser ? <Route {...props} /> : <Redirect to="/login-register" /> 
        }

        
    }

export default SecureRoute;