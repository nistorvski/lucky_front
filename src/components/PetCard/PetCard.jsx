import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Step1 from "./Step1/Step1";
import Step2 from "./Step2/Step2";
import Step3 from "./Step3/Step3";

import "./PetCard.scss";

const PetCard = (props) => {
  if (!props.location.state) props.history.push("/pets");

  const [pet, setPet] = useState({ ...props.location.state });
  const [page, setPage] = useState("Step1");

  console.log(props);

  return (
    <div className="petcard">
      <img className="petcard__img" src={pet.photo} />
      <div className="petcard__menu">
        <div className="petcard__submenu">
          <img className="petcard__male" src={process.env.PUBLIC_URL + "/male.png"} alt="" />
          <div className="petcard__name">
            <p>{pet.name}</p>
            <p>{pet.zone}</p>
          </div>
        </div>
        <div className="">
          <img src={process.env.PUBLIC_URL + "/favoritos.png"} alt="" />
          <img src={process.env.PUBLIC_URL + "/compartir.png"} alt="" />
        </div>
      </div>

      <div className="petcard__card">
        <div className="petcard__index">
          <button className="petcard__button" onClick={() => setPage("Step1")}>Detalles</button>
          <button className="petcard__button" onClick={() => setPage("Step2")}>Salud</button>
          <button className="petcard__button" onClick={() => setPage("Step3")}>Adopción</button>
        </div>
        
        <div className="petcard__container">
          {page === "Step1" && <Step1 props={{ state: pet }} />}
          {page === "Step2" && <Step2 props={{ state: pet }} />}
          {page === "Step3" && <Step3 props={{ state: pet }} />}
        </div>
      </div>
      <Link to={"/pets"}>
        <button>Apadrinar</button>
      </Link>
      <Link to={"/pets"}>
        <button>Adoptar</button>
      </Link>
    </div>
  );
};

export default PetCard;
