import React, { useState } from "react";

import './Step1.scss';

const Step1 = (props) => {

    const [pet, setPet] = useState({...props.props.state});

    console.log(props.props.state)
  return (
    <div className="step1">
      <div className="step1__details">
        <div className="step1__info">
          <img className="step1__icon" src={process.env.PUBLIC_URL + "/pawprint.png"} alt=""/>
          <p>Especie</p>
        </div>
        <p>{pet.name}</p>
      </div>
      <div className="step1__details">
        <div className="step1__info">
          <img className="step1__icon"  src={process.env.PUBLIC_URL + "/pawprint.png"} alt="" />
          <p>Fecha de nacimiento</p>
        </div>
        <p>{pet.dateBorn}</p>
      </div>
      <div className="step1__details">
        <div className="step1__info">
          <img className="step1__icon"  src={process.env.PUBLIC_URL + "/pawprint.png"} alt=""/>
          <p>Raza</p>
        </div>
        <p>{pet.breed}</p>
      </div>
      <div className="step1__details">
        <div className="step1__info">
          <img className="step1__icon"  src={process.env.PUBLIC_URL + "/pawprint.png"} alt=""/>
          <p>Tamaño</p>
        </div>
        <p>{pet.Size}</p>
      </div>
      <div className="step1__details">
        <div className="step1__info">
          <img className="step1__icon"  src={process.env.PUBLIC_URL + "/pawprint.png"} alt=""/>
          <p>Peso</p>
        </div>
        <p>{pet.weight}</p>
      </div>
    </div>
  );
};

export default Step1;
