import React, { useState } from "react";

import "./Step2.scss";

const Step2 = (props) => {
  const [pet, setPet] = useState({ ...props.props.state });

  console.log(props.props.state);
  return (
    <div className="step2">
      <div className="step2__details">
        <div className="step2__info">
          <img
            className="step2__icon"
            src={process.env.PUBLIC_URL + "/pawprint.png"}
            alt=""
          />
          <p>Vacunado</p>
        </div>
        <p>{pet.vaccinated}</p>
      </div>
      <div className="step2__details">
        <div className="step2__info">
          <img
            className="step2__icon"
            src={process.env.PUBLIC_URL + "/pawprint.png"}
            alt=""
          />
          <p>Desparasitado</p>
        </div>
        <p>{pet.dewormed}</p>
      </div>
      <div className="step2__details">
        <div className="step2__info">
          <img
            className="step2__icon"
            src={process.env.PUBLIC_URL + "/pawprint.png"}
            alt=""
          />
          <p>Zona</p>
        </div>
        <p>{pet.zone}</p>
      </div>
      <div className="step2__details">
        <div className="step2__info">
          <img
            className="step2__icon"
            src={process.env.PUBLIC_URL + "/pawprint.png"}
            alt=""
          />
          <p>Microchip</p>
        </div>
        <p>{pet.chip}</p>
      </div>
      <div className="step2__toknow">
        <p>Tienes que saber que</p>
      </div>
    </div>
  );
};

export default Step2;
