import React, { useState } from "react";

import "./Step3.scss";

const Step3 = (props) => {
  const [pet, setPet] = useState({ ...props.props.state });

  console.log(props.props.state);
  return (
    <div className="step3">
      <div className="step3__details">
        <h5>Requisitos adopción</h5>
        <p>No hay requisitos especiales para adoptar a Blue</p>
      </div>
      <div className="step3__details">
        <div className="step3__info">
          <h5>Tasa de adopción</h5>
          <img
            className="step3__icon"
            src={process.env.PUBLIC_URL + "/ayuda.png"}
            alt=""
          />
        </div>
        <p>125€</p>
      </div>
      <div className="step3__details">
        <h5>¿Se envía a otra ciudad?</h5>
        <p>No se envía a otra ciudad</p>
      </div>
    </div>
  );
};

export default Step3;
