import React from "react";
import "./FormFamily.scss";
import { Link } from "react-router-dom";
import FormPets from "../FormPets/FormPets";
import { useState } from "react";
import { setState } from "react";

function FormFamily() {
  const handleSubmitForm = (ev) => {
    ev.preventDefault();
  };
  function onChangeInput(ev) {
    const { name, value } = ev.target;
    this.setState({ [name]: value });
  }
  const handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  return (
    <form onSubmit={handleSubmitForm}>
      <div>
        <div>
          <h3>Formulario de adopcion</h3>
        </div>
        <div className="barra">
          <div className="barra_carga_family"></div>

          <div className="barra_carga_family2"></div>
        </div>

        <div className="family_hogar">
          <h2>Familia y hogar</h2>
        </div>
        <div>
          <label>
            <p>¿donde Vives</p>
            <input
              type="text"
              onChangeInput={handleInputChange}
              placeholder="piso, casa, chalet"
            />
          </label>
          <div>
            <label>
              <p>
                ¿vives de alquiler?
                <div className="si-no">
                  <div className="si-no__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="si-no__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿Tu casero permite animales?
                <div className="caseros">
                  <div className="caseros__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="caseros__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿crees que podrias mudarte pronto?
                <div className="mudarte">
                  <div className="mudarte__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="mudarte__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿tiene jardin?
                <div className="jardin">
                  <div className="jardin__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="jardin__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿vives con otra personas?
                <div className="FormFamily">
                  <div className="FormFamily__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>

                  <div className="FormFamily__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿estan deacuerdo con la adopcion?
                <div className="adopcion">
                  <div className="adopcion__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="adopcion__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <label>
              <p>
                ¿estas deacuerdo con que visitemos tu casa?
                <div className="visithouse">
                  <div className="visithouse__no">
                    no
                    <input
                      type="radio"
                      name="y_n"
                      value="no"
                      placeholder="no"
                    />
                  </div>
                  <div className="visithouse__si">
                    si
                    <input
                      type="radio"
                      name="y_n"
                      value="si"
                      placeholder="si"
                    />
                  </div>
                </div>
              </p>
            </label>
            <Link to="formPets">
              <button onKeyPress={() => this.props.navigation.goBack(FormPets)}>
                Previous
              </button>
            </Link>

            <button className="enviar" type="submit">
              enviar
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default FormFamily;
