import React from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  Image,
} from "pure-react-carousel";
import "./Gallery.scss";

import "pure-react-carousel/dist/react-carousel.es.css";

function Gallery() {
  return (
    <div>
      <CarouselProvider
        naturalSlideWidth={300}
        naturalSlideHeight={800}
        totalSlides={3}
        // infinite={false}
      >
        <Slider>
          <Slide className="carousel-gallery">
            {" "}
            <img src="/img/imagen1.png" width=" 250px" height="500px" alt="" />
            <h3>Encuentra todo tipo de servicios cerca de ti</h3>
          </Slide>
          <Slide className=" carousel-adoption">
            {" "}
            <img
              src="/img/imagenadopcion.png"
              width=" 250px"
              height="500px"
              alt=""
            />
            <h3>Adopta desde tu movil</h3>
            <p>
              Puedes accerder al perfil de muchos aimales que estan en adopción
              y filtrarlos para encontrar el que mejor se adapta a ti
            </p>
          </Slide>
          <Slide className="carousel-final">
            {" "}
            <img
              src="/img/imagenfinal.png"
              width=" 250px"
              height="500px"
              alt=""
            />
            <h3>
              Si eres un asociación sube a tus pèludos para darles mas difusión
            </h3>
          </Slide>

          {/* <ButtonBack>Back</ButtonBack>
        <ButtonNext>Next</ButtonNext> */}
        </Slider>
      </CarouselProvider>
    </div>
  );
}

export default Gallery;
