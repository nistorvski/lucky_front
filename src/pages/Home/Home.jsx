import React from "react";
import "./Home.scss";
import { Link } from "react-router-dom";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

const footerLinks = [
  <img src="/img/home.png" width="15px" height="15px" alt="" />,
  <img src="/img/mapa.png" width="15px" height="35px" alt="" />,
  <img src="img/mascota.png" width="15px" height="35px" alt="" />,
  <img src="/img/perfil.png" width="15px" height="35px" alt="" />,
];

function Home() {
  return (
    <div>
      <div>
        <div>
          <div className="hello-home">
            <h3>¡Hola Celia! </h3>{" "}
          </div>
          <div className="carouselprovider">
            <CarouselProvider
              naturalSlideWidth={200}
              naturalSlideHeight={140}
              totalSlides={3}
            >
              <Slider className="slides">
                <Link to="petslist">
                  <Slide className="carousel-inner">
                    {" "}
                    <img src="/img/mascota.png" alt="" />
                    <h3>Mascotas</h3>
                    <p>Mira y elige entre nuestras mascotas</p>
                  </Slide>
                </Link>
                <Link to="progress">
                  <Slide className="carousel-inner">
                    {" "}
                    <img src="/img/eventos.png" alt="" />
                    <h3>Progreso de adopcion</h3>{" "}
                    <p>Sigue los progresos de tu adopcion</p>
                  </Slide>
                </Link>
                <Link to="<shelter/>">
                  <Slide className="carousel-inner">
                    {" "}
                    <img src="/img/protectora.png" alt="" />
                    <h3>Protectoras</h3>
                    <p>Nuestras protectoras</p>
                  </Slide>
                </Link>
              </Slider>

              {/* <ButtonBack>Back</ButtonBack>
              <ButtonNext>Next</ButtonNext> */}
            </CarouselProvider>
          </div>
        </div>
        <hr />

        <div>
          <div className="novedades">
            <div className="box">
              <Link to="/pets">
                <img src="/img/uli1Copy.png" alt="chinchillas" />
              </Link>

              <h5>observa a nuestros peludos</h5>
            </div>
            <div className="box">
              <Link to="/novedades">
                <img src="/img/uli3Copy.png" alt="una iguana" />
              </Link>
              <h5>Nuestras novedades en animales</h5>
            </div>
            <div className="box">
              <Link to="/protectoras">
                <img src="/img/uli2Copy.png" alt="perro motorista" />
              </Link>
              <h5>tendras buenos amigos</h5>
            </div>

            <div className="footerLinks">
              <ul>
                {footerLinks.map((linkName) => {
                  return <li key={linkName}>{linkName}</li>;
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
