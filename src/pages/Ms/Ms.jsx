import React, { useState } from 'react';
import './Ms.scss';
import { Redirect, Link } from 'react-router-dom';
import { logout } from "../../api/auth";
import profileIcon from '../../images/icons/shelters/protectora@3x.png';
import locationIcon from '../../images/icons/envent/eventos@3x.png';
import favoritosIcon from '../../images/icons/blog/blogCopy@3x.png';
import notificationIcon from '../../images/icons/help/ayuda@3x.png';
import adoptionIcon from '../../images/icons/config/confi@3x.png';
import donationIcon from '../../images/icons/exit/salir@3x.png';

import Navbar from '../../components/Navbar/Navbar';



const Ms = (props) => {

    const [error, setError] = useState("");


    return (
        <section className="ms">


            <ul className="ms__list">
                <li className="ms__list__item"> <img className="ms__list__icon-left" src={profileIcon} alt="" /> Asociaciones protectoras </li>
                <li className="ms__list__item"> <img className="ms__list__icon-left" src={locationIcon} alt="" /> Eventos   </li>
                <li className="ms__list__item"> <img className="ms__list__icon-left" src={favoritosIcon} alt="" /> Curiosidades  </li>
                <li className="ms__list__item"> <img className="ms__list__icon-left" src={notificationIcon} alt="" /> Ayuda  </li>
                <li className="ms__list__item"> <img className="ms__list__icon-left" src={adoptionIcon} alt="" /> Configuración  </li>
            </ul>

            <div className="ms__list__separation"></div>

            <Link to="/register-choose">
                <div className="ms__list__item ms__list__item-exit" onClick={logout} >
                    <img className="ms__list__icon-left" src={donationIcon} alt="icon" />Logout
                </div>
            </Link>

            <Navbar user={props.user} />

        </section>
    )
}


export default Ms;