import React from 'react';
import './Profile.scss';

import profileIcon from '../../images/icons/perfil/chica@3x.png';
import locationIcon from '../../images/icons/localization/localization@3x.png';
import favoritosIcon from '../../images/icons/favoritos/favoritosCopy@3x.png';
import notificationIcon from '../../images/icons/notificaciones/notificaciones@3x.png';
import adoptionIcon from '../../images/icons/mascotaicon/mascota@3x.png';
import donationIcon from '../../images/icons/donar/donar@3x.png';

import Navbar from '../../components/Navbar/Navbar';


const Profile = (props) => {

    const { avatar } = props.user;

    return (
        <section className="profile">

            <div className="profile__avatar" >
                <img className="profile__avatar__image" className="profile__avatar__image" src={avatar} alt="" />
            </div>


            <div className="profile__list__separation">
                <ul className="profile__list">
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={profileIcon} alt="" /> Mi perfil </li>
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={locationIcon} alt="" /> Direcciones   </li>
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={favoritosIcon} alt="" /> Favoritos  </li>
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={notificationIcon} alt="" /> Notificaciones  </li>
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={adoptionIcon} alt="" /> Estado de la adopcion  </li>
                    <li className="profile__list__item"> <img className="profile__list__icon-left" src={donationIcon} alt="" /> Donar  </li>
                </ul>
            </div>


            <Navbar user={props.user} />

        </section>
    )
}


export default Profile;