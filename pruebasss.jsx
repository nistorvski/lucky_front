

const handleProfessorForm = (event) => {
    const { name, value } = event.target;
   
    if (name === 'mail'){
        setMailState(value);
    }else if (name === 'name'){
        setNameState(value);
    }else if (name === 'age'){
        setAgeState(value)
    }else if (name === 'lastName'){
        setLastnameState(value)
    }else if (name === 'photo'){
        let reader = new FileReader();
        let file = event.target.files[0];
     
        reader.onloadend = () => {
            setPhoto(file)
            setPhotoPreview(reader.result)
            }
            reader.readAsDataURL(file)

            console.log(file)
   
    }else if(name === 'password'){
        setPassword(value)
    }
}





const submitForm = async(event) => {
    setModalOpen(!isModalOpen);
    event.preventDefault();
    const form = new FormData();
        form.append('photo', photoState)     
        form.append('name', nameState);
        form.append('lastName', lastnameState);
        form.append('age', ageState);
        form.append('mail', mailState);
        form.append('password', passwordState);

    const data = await createStudentService(form);

    toggleModal();
}





<form className="b-form" onSubmit={submitForm} method="POST" enctype="multipart/form-data">
                <fieldset className="b-form__field">
                    <legend>Datos personales</legend>
                    {photoPreview && <img className="b-form__"src={photoPreview} alt="student"></img>}
                    <label for="photo">Photo:</label>
                    <input type="file" 
                        name="photo"
                        onChange={handleProfessorForm}  
                        accept="image/png, image/jpg, image/jpeg"/>
                
                    <label for="name">Name: </label>
                    <input type="text" 
                        name="name"
                        onChange={handleProfessorForm} 
                        value={nameState}></input>
                
                    <label for="lastName">Lastname: </label>
                    <input name="lastName" 
                            type="text" 
                            onChange={handleProfessorForm}
                            value={lastnameState}></input>
                
                    <label for="mail">Email: </label>
                    <input name="mail" 
                        type="text"
                        onChange={handleProfessorForm}
                        value={mailState}></input>
                    
                    <label for="age">Age: </label>
                    <input name="age" 
                        type="number"
                        onChange={handleProfessorForm}  
                        value={ageState}></input>

                    <label for="password">Password</label>
                    <input type="password"
                            name="password"
                            onChange={handleProfessorForm}
                            value={passwordState}></input>
                            
                </fieldset>





  export const createStudentService = async student => {
  const request = await fetch(newStudentURL, {
    method: "POST",
    headers: {
      "Accept": "*",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: student
})

const response = await request.json();
if(!request.ok) {
  return new Error(response.message);
}
return response;
}



////////////////////////////////////////////


{/* 

      // fileSelectedHandler = ev => {
      //   this.setState({
      //     selectedFile: ev.target.files[0]
      //   });
    
      //   console.log(ev.target.files[0]);
      // };

      // fileUploadHandler = () => {
      //   const formData = new FormData();

      //   formData.append("file", this.state.selectedFile);
      //   //formData.append("tags", `codeinfuse, medium, gist`);
      //   formData.append("upload_preset", `${this.state.selectedFile.name}-${new Date().getTime()}`); // Replace the preset name with your own
      //   formData.append("api_key", "731398272159567"); // Replace API key with your own Cloudinary key
      //   formData.append("timestamp", (Date.now() / 1000) | 0);

      //   //fd.append('avatar',  this.state.selectedFile, `${this.state.selectedFile.name}-${new Date().getTime()} `);
      //   axios.post('https://api.cloudinary.com/v1_1/dxe18ekdp/image/upload', formData)
      //   .then(res => {
      //     console.log(res);
      //   })
      //   .catch(error => {
      //       console.log(error.response)
      //   });
      // }
     */}



{/* 
     ////////////////////////////////////////// */}






import {Component, useState} from'react';
import {register} from '../../api/auth';
//import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faImages } from '@fortawesome/free-solid-svg-icons'

import Buttons from '../../components/Buttons/Buttons';
import './Register.scss';


function UserRegister()  {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [avatar, setAvatar] = useState('');
    const [previewAvatar, setAvatarPreview] = useState("");
    const [error, setError]= useState('');
    const [fileInput, setFileInput] = useState(fileInput);

    
    
    const handleChangeInput = (ev) => {
        const { name, value, } = ev.target;
        this.setState({
          [name]: value,
        });
        if(name === "avatar"){
          let reader = new FileReader();
          let file = ev.target.files[0];

          reader.onloadend = () => {
            this.setState({
              setAvatar:file,
              setAvatarPreview: reader.result
            })
          }
          reader.readAsDataURL(file);

          console.log(file);

        }
      };

    
    
   const handleFormSubmit = async (ev) => {
        ev.preventDefault();
        try{

            const form = new FormData();

              form.append('avatar', avatar)     
              form.append('name', name);
              form.append('email', email);
              form.append('password', password);

            
            const data = await register (form);

            //props.saveUser(data);
            // this.setState({
              
            // });
            console.log("REgistro completadoo", data);

        }catch(error) {
            this.setState({
                error: error.message,
            });
        }
    };

    const fileInput = () => setFileInput fileInput ;

        return(
            <div className="register">

                <div className="register__logo">
                    <img src="/images/logo_img/logo.png" alt=""/>
                </div>

                <div className="register__title">
                    <p>¡Hola! para continuar, sigue los pasos para crear una cuenta</p>
                </div>

                <form onSubmit={handleFormSubmit} className="register__form" >

                <label htmlFor="name" className="register__form__label">
                            <input 
                            className="register__form__input" 
                            type="text"
                            name="name"
                            placeholder="Username"
                            value={name}
                            onChange={handleChangeInput}
                            /> 
                    </label>


                    <label htmlFor="email" className="register__form__label">
                            <input className="register__form__input"
                             type="email"
                             name="email"
                              placeholder="Email"
                              value={email}
                              onChange={handleChangeInput}
                              /> 
                    </label>
               
                    <label htmlFor="password" className="register__form__label">
                        <input className="register__form__input"
                         type="password"
                         name="password"
                          placeholder="Password"
                          value={password}
                          onChange={handleChangeInput}
                          />
                    </label>

                    <label htmlFor="avatar" className="register__form__label">
                        <input
                        style={{display:'none'}}
                            className="register__form__input"
                            type="file"
                            name="avatar"
                            placeholder="Avatar"
                            value={avatar}
                            onChange={handleChangeInput}
                            ref={fileInput}
                          />

                          <FontAwesomeIcon onClick={() => this.fileInput.click()}  className="register__form__file" icon={faImages} /> 
                          <p className="register__form__paragraf"  >Añade avatar</p>  
               

                    </label>


                    <Buttons type="submit" style={"blue"} children={"Registrar mi cuenta"} />

                </form>               

            </div>
        )
    }

export default UserRegister;